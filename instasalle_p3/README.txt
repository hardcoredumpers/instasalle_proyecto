Debido a la falta de tiempo, algunas funcionalidades no se han acabado de realizar por
el motivo en que se ha centrado principalmente en implementar las estructuras que
cre�amos m�s asequibles con el tiempo que dispon�amos habiendo entendido su funcionamiento:
Graphs, AVL, Trie, Tabla Hash a la vez que se trataba de entender cada concepto
en su totalidad. 

Estado de las funcionalidades:

- Grafos: funciona la operaci�n de inserci�n, eliminaci�n y visualizaci�n de la estructura. Se puede visualizar la estructura por pantalla.

- Trie: funciona la operaci�n de inserci�n, eliminaci�n, autocompletar pero sin la aplicaci�n del l�mite de autocompletar. No se puede visualizar la estructura por pantalla.

- AVL: est� todo implementado y como resultado de las pruebas, funciona la operaci�n de inserci�n y b�squeda pero falla en la eliminaci�n de un nodo. Se elimina todos los dem�s nodos a partir del nodo que hemos eliminado. Se puede visualizar el orden de los posts (inordre) pero no el contenido de cada post.

- Tabla Hash: todo est� implementado pero en las pruebas no funciona correctamente la inserci�n, por tanto, no hemos podido probar la eliminaci�n y la b�squeda. No se puede visualizar la estructura por pantalla.

- R-Tree: No se ha implementado ninguna operaci�n. Hemos considerado de realizar esta estructura como la �ltima de todas por su complejidad adem�s de que decidimos entender el concepto en su totalidad.

Respecto a la importaci�n y exportaci�n de ficheros, el programa permite importar los datos de ficheros JSON pero no est� implementado la exportaci�n.

Para poder testear el programa, hay que abrir la soluci�n del proyecto en Visual Studio. Es importante tener el proyecto abierto dentro de la soluci�n. Pulsa "Start" y se abrir� una Consola donde se ver� el Menu principal.




