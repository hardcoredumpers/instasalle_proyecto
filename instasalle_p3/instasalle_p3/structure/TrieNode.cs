﻿namespace instasalle_p3.structure
{
    public class TrieNode
    {
        public const int TOTAL_ALPHABET = 26; // words are in ENGLISH
        public TrieNode[] children;
        public bool endWord;

        public TrieNode()
        {
            this.children = new TrieNode[TOTAL_ALPHABET];
            this.endWord = false;
        }
    }
}