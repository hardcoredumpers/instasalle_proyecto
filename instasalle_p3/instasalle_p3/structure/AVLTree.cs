﻿using instasalle_p3.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p3.structure
{
    class AVLTree
    { 
        public AVLNode root;

        public AVLTree(AVLNode root)
        {
            this.root = root;
        }

        public AVLTree()
        {
            root = null;
        }

        // Desbalanceig
        public AVLNode RightRotate(AVLNode currentNode)
        {
            AVLNode newRootNode = currentNode.left;
            AVLNode subRight = newRootNode.right; // subtree to the right

            // Rotate to the right
            newRootNode.right = currentNode; // makes it the top node
            currentNode.left = subRight; // puts the subtree to the left side of the currentNode

            // Calculate new depths
            currentNode.depth = CalculateDepth(currentNode) + 1;
            newRootNode.depth = CalculateDepth(newRootNode) + 1;

            return newRootNode;
        }

        public AVLNode LeftRotate(AVLNode currentNode)
        {
            AVLNode newRootNode = currentNode.right;
            AVLNode subLeft = newRootNode.left;

            // Rotate to the left
            newRootNode.left = currentNode;
            currentNode.right = subLeft;

            // Calculate new depths
            currentNode.depth = CalculateDepth(currentNode) + 1;
            newRootNode.depth = CalculateDepth(currentNode) + 1;

            return newRootNode;
        }

        public void InsertAVL(Post post)
        {
            root = Insert(root, post);
            
        }

        private AVLNode Insert(AVLNode rootNode, Post post)
        {
            if (rootNode == null)
            {
                rootNode = new AVLNode(post);
                return rootNode;
            } else
            {
                // Perform normal RBT Insert
                // Go left 
                if (post.id < rootNode.post.id)
                {
                    rootNode.left = Insert(rootNode.left, post);
                } else
                {
                    // Go right
                    if (post.id > rootNode.post.id)
                    {
                        rootNode.right = Insert(rootNode.right, post);
                    }
                }

                // Update the height of the node
                rootNode.depth = CalculateDepth(rootNode) + 1;

                // Calculate the balance factor
                int balanceFactor = CalculateBalanceFactor(rootNode);

                // Check for imbalance (4 cases)
                
                if (balanceFactor > 1)
                {
                    // Left Left
                    if (post.id < rootNode.left.post.id)
                    {
                        return RightRotate(rootNode);
                    }
                    // Left Right
                    else if (post.id > rootNode.left.post.id)
                    {
                        rootNode.left = LeftRotate(rootNode.left);
                        return RightRotate(rootNode);
                    }
                }

                if (balanceFactor < -1) {
                    // Right Right
                    if (post.id > rootNode.right.post.id)
                    {
                        return LeftRotate(rootNode);
                    }
                    // Right Left
                    else if (post.id < rootNode.right.post.id)
                    {
                        rootNode.right = RightRotate(rootNode.right);
                        return LeftRotate(rootNode);
                    }
                }

                // If everything is balanced, it will return the same node.
                return rootNode;
            }
        }

        public void DeleteAVL(Post post)
        {
            Delete(root, post);
            
        }

        private int Delete(AVLNode rootNode, Post post)
        {
            int deleteCase = 0;
            // If the post has been found
            if (rootNode.post.id == post.id)
            {
                // First case: Leaf node
                if (rootNode.left == null && rootNode.right == null)
                {
                    deleteCase = 1;
                }
                // Second case a): Just one child - left
                else if (rootNode.left != null && rootNode.right == null)
                {
                    deleteCase = 2;
                }
                // Second case b): Just one child - right
                else if (rootNode.right != null && rootNode.left == null)
                {
                    deleteCase = 3;
                }
                // Last case: Two children - both left and right
                else
                {
                    deleteCase = 0;
                }
                return deleteCase;

            }
            else
            {
                // Go left if the node has a left child
                if (post.id < rootNode.post.id && rootNode.left != null)
                {
                    deleteCase = Delete(rootNode.left, post);

                    switch (deleteCase)
                    {
                        case 1:
                            rootNode.left = null;
                            break;
                        case 2:
                            rootNode.left = rootNode.left.left;
                            break;
                        case 3:
                            rootNode.left = rootNode.left.right;
                            break;
                        case 0:
                            // Get the next inorder successor
                            AVLNode tempNode = GetNextInorderSuccessor(rootNode.right);

                            // Set tempNode as the rootNode's left child
                            rootNode.left = tempNode;

                            // Delete the original inorder successor
                            Delete(rootNode.right, tempNode.post);

                            break;
                    }
                }
                // Go right if the node has a right child
                else if (post.id > rootNode.post.id && rootNode.right != null)
                {
                    deleteCase = Delete(rootNode.right, post);

                    switch (deleteCase)
                    {
                        case 1:
                            rootNode.right = null;
                            break;
                        case 2:
                            rootNode.right = rootNode.right.left;
                            break;
                        case 3:
                            rootNode.right = rootNode.right.right;
                            break;
                        case 0:
                            // Get the next inorder successor
                            AVLNode tempNode = GetNextInorderSuccessor(rootNode.right);

                            // Set tempNode as the rootNode's left child
                            rootNode.right = tempNode;

                            // Delete the original inorder successor
                            Delete(rootNode.right, tempNode.post);

                            break;
                    }
                }
            }
            return deleteCase;
        }

        public bool SearchAVL(AVLTree tree, Post post)
        {
            // If the tree is empty
            if (tree.root == null)
            {
                return false;
            } else
            {
                // If the tree is not empty, search the post
                return Search(tree.root, post);
            }
        }

        private bool Search(AVLNode rootNode, Post post)
        {
            if (post.id == rootNode.post.id)
            {
                return true;
            } else
            {
                if (post.id < rootNode.post.id && rootNode.left != null)
                {
                    return Search(rootNode.left, post);
                } else
                {
                    if (post.id > rootNode.post.id && rootNode.right != null)
                    {
                        return Search(rootNode.right, post);
                    }
                }
                return false;
            }
        }

        public void Destroy()
        {
            root = null;
        }

        public void showInorder(AVLNode rootNode)
        {
            if (rootNode != null)
            {
                showInorder(rootNode.left);
                Console.WriteLine(rootNode.post.id);
                showInorder(rootNode.right);
            }
        }

        public AVLNode GetNextInorderSuccessor(AVLNode rootNode)
        {
            if (rootNode != null)
            {
                return GetNextInorderSuccessor(rootNode.left);
            } else
            {
                return rootNode;
            }
        }

        // Calculates the depth of the tree starting from the indicated node recursively.
        public int CalculateDepth(AVLNode node)
        {
            if (node == null)
                return 0;
            else
                return Math.Max(CalculateDepth(node.left), CalculateDepth(node.right)) + 1;
        }

        // Calculates the balance factor from a certain node in a tree.
        public int CalculateBalanceFactor(AVLNode node)
        {
            if (node == null)
            {
                return 0;
            }
            return CalculateDepth(node.left) - CalculateDepth(node.right);
        }

        public Post GetLastPost(AVLNode rootNode, Post lastPost)
        {
            if (root == null)
            {
                return null;
            } else
            {
                if (rootNode.right != null)
                {
                    lastPost = GetLastPost(rootNode.right, lastPost);
                }
                else if (rootNode.left != null)
                {
                    lastPost = GetLastPost(rootNode.left, lastPost);

                }
                else
                {
                    return (Post)rootNode.post;
                }
            }
            return lastPost;
        }
    }
}
