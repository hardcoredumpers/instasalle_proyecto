﻿using instasalle_p3.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p3.structure
{
    class Menu
    {
        public void printVisualización()
        {
            Console.WriteLine("\n----Visualización de la estructura----");
            Console.WriteLine("¿Qué estructura desea visualizar?");
            Console.WriteLine("\t1. Trie");
            Console.WriteLine("\t2. R-Tree");
            Console.WriteLine("\t3. AVL Tree");
            Console.WriteLine("\t4. Taula de Hash");
            Console.WriteLine("\t5. Graph");
            Console.Write("> ");
        }

        public void printFunctionalities(int option)
        {
            Functionalities mainInstance = Functionalities.Instance;
            switch (option)
            {
                case 1:
                    Console.WriteLine("\n-----------------------Importación de ficheros----------------------");
                    Console.WriteLine("Especificar ruta del fichero a importar correspondiente a los usuarios");
                    Console.Write("> ");
                    string strUsers = "\\" + Console.ReadLine().ToString();
                    mainInstance.LoadUsers(strUsers);

                    Console.WriteLine("Especificar ruta del fichero a importar correspondiente a los posts");
                    Console.Write("> ");
                    string strPosts = "\\" + Console.ReadLine().ToString();
                    mainInstance.LoadPosts(strPosts);
                    
                    //Console.WriteLine("\nFicheros cargados correctamente.");

                    break;
                case 2:
                    printExport();
                    int option2 = Convert.ToInt32(Console.ReadLine());
                    switch (option2)
                    {
                        case 1:
                            Console.WriteLine("\nEspecificar la ruta del fichero correspondiente a usuarios");
                            Console.Write("> ");
                            String strUsersExport = Convert.ToString(Console.ReadLine());
                            Console.WriteLine("Especificar la ruta del fichero correspondiente a posts");
                            Console.Write("> ");
                            String strPostsExport = Convert.ToString(Console.ReadLine());
                            break;

                        case 2:
                            Console.WriteLine("\nRuta del fichero a exportar correspondiente a Tries");
                            Console.Write("> ");
                            String strExportTries = Convert.ToString(Console.ReadLine());
                            Console.WriteLine("Ruta del fichero a exportar correspondiente a R-Trees");
                            Console.Write("> ");
                            String strExportRtrees = Convert.ToString(Console.ReadLine());
                            Console.WriteLine("Ruta del fichero a exportar correspondiente a AVL Tree");
                            Console.Write("> ");
                            String strExportAVL = Convert.ToString(Console.ReadLine());
                            Console.WriteLine("Ruta del fichero a exportar correspondiente a Hash table");
                            Console.Write("> ");
                            String strExportHash = Convert.ToString(Console.ReadLine());
                            Console.WriteLine("Ruta del fichero a exportar correspondiente a Graph");
                            Console.Write("> ");
                            String strExportGraph = Convert.ToString(Console.ReadLine());
                            // TODO: Export as JSON
                            break;
                    }
                    break;
                case 3:
                    printVisualización();
                    
                    int opcionVisualizar = Convert.ToInt32(Console.ReadLine());
                    switch(opcionVisualizar)
                    {
                        case 1: // Trie
                            mainInstance.VisualizeTrie();
                            break;
                        case 2: // R-Tree
                            mainInstance.VisualizeRTree();
                            break;
                        case 3: // AVL Tree
                            mainInstance.VisualizeAVL();
                            break;
                        case 4: // Hash table
                            mainInstance.VisualizeAVL();
                            break;
                        case 5: // Graph
                            mainInstance.VisualizeGraph();
                            break;
                    }
                    break;
                case 4:
                    printInserir();
                    string optionFollowers;
                    int opcionInserir = Convert.ToInt32(Console.ReadLine());
                    switch(opcionInserir)
                    {
                        case 1:
                            mainInstance.AddNewUserToGraph();
                            break;
                        case 2:
                            mainInstance.AddNewPost();
                            break;
                    }
                    break;
                case 5:
                    printEliminar();
                    int opcionEliminar = Convert.ToInt32(Console.ReadLine());
                    switch (opcionEliminar)
                    {
                        case 1:
                            mainInstance.RemoveUserFromGraph();
                            break;
                        case 2:
                            mainInstance.RemovePost();
                            break;
                    }
                    break;
                case 6:
                    printCerca();
                    int opcionCerca = Convert.ToInt32(Console.ReadLine());
                    switch (opcionCerca)
                    {
                        case 1:
                            //Console.Write("\n> ");
                            //String strCerca = Convert.ToString(Console.ReadLine());
                            //Console.Write("Posibles suggerencias: ");
                            mainInstance.PrintSuggestions();
                            break;
                        case 4: // TODO: Apply R-Tree
                            Console.Write("\nLatitud: \n> ");
                            double latitud = Convert.ToDouble(Console.ReadLine());
                            Console.Write("Longitud: \n> ");
                            double longitud = Convert.ToDouble(Console.ReadLine());
                            Console.Write("Radio máximo: \n> ");
                            int radio = Convert.ToInt32(Console.ReadLine());
                            break;
                    }
                    break;
                case 7:
                    Console.Write("\n---------Limitar memoria para autocompletar----------");
                    mainInstance.updateActualLimit();
                    // TODO: Add this limit to the implementation of Autocomplete in Trie
                    break;

            }
        }

        public void printMenu ()
        {
            //Console.Write("1. Trie\n2. R-Tree\n3. AVL Tree\n4. Taula de Hash\n5. Graph\n\n6. Exit");
            Console.Write("1. Importación de ficheros\n2. Exportación de ficheros\n3. Visualización de la estructura\n4. Inserción de información\n5. Eliminación de información\n6. Búsqueda de información\n7. Limitar memoria para autocompletar");
            Console.Write("\n\n8. Salir\n\nOpción: ");
        }

        public void printCerca ()
        {
            Console.WriteLine("\n-----------Cerca información-----------");
            Console.WriteLine("¿Qué tipo de información quiere buscar?");
            Console.WriteLine("1. Usuario\n2. Post\n3. Según hashtag \n4. Según ubicación");
            Console.Write("> ");
        }

        public void printEliminar ()
        {
            Console.WriteLine("\n-----------Eliminar información-----------");
            Console.WriteLine("¿Qué tipo de información quiere buscar?");
            Console.WriteLine("1. Usuario\n2. Post");
            Console.Write("> ");

        }

        public void printInserir()
        {
            Console.WriteLine("\n----------Inserción de información-----------");
            Console.WriteLine("¿Qué tipo de información quiere inserir?");
            Console.WriteLine("1. Usuario\n2. Post");
            Console.Write("> ");
        }

        public void printExport ()
        {
            Console.WriteLine("\n-------------------Exportación ficheros------------------");
            Console.WriteLine("1. Exportación de ficheros en formato usuarios y posts");
            Console.WriteLine("2. Exportación de ficheros en formato todas las estructuras");
            Console.Write("> ");
        }

        public bool isBack (int option)
        {
            return option == 6;

        }

        public bool isExit (int option)
        {
            return option == 8;
        }

        public void printOptionError ()
        {
            Console.WriteLine("Por favor, introduce una opción válida.");
        }
            
    }
}
