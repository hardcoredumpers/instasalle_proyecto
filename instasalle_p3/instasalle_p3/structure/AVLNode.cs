﻿using instasalle_p3.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p3.structure
{
    class AVLNode
    {
        public AVLNode left;
        public AVLNode right;
        public AVLNode parent;
        public Post post;
        public int balanceFactor;
        public int depth;

        public AVLNode(Post post, AVLNode left, AVLNode right, AVLNode parent)
        {
            this.left = left;
            this.right = right;
            this.parent = parent;
            this.post = post;
        }

        public AVLNode(Post post)
        {
            this.left = null;
            this.right = null;
            this.parent = null;
            this.post = post;
        }

        // Calculates the balance factor from a certain node in a tree.
        public int calculateBalanceFactor_DEPRECATED()
        {
            int balance = 0;
            // Get the height of the tree to the left
            int heightLeft = 0;

            // Get the height of the tree to the right
            int heightRight = 0;

            // Subtract the two values
            balance = Math.Abs(heightLeft - heightRight);

            return balance;
        }
        
    }
    
}
