﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using instasalle_p3.model;


namespace instasalle_p3.structure
{
    class HashTable<String, Post>
    {
        public ArrayList arrayHashtags;
        public int numHashtags;
        public int sizeArray;

        public HashTable()
        {
            arrayHashtags = new ArrayList();
            numHashtags = 20;
            sizeArray = 0;

            // Initialize buckets
            for (int i = 0; i < numHashtags; i++)
            {
                arrayHashtags.Add(null);
            }
        }

        public bool IsEmpty()
        {
            return sizeArray == 0;
        }

        public int GetHashtagIndex(String key)
        {
            
            return key.ToString().Length % numHashtags;
        }

        public model.Post RemoveHashtag(String hashtag)
        {
            int hashtagIndex = GetHashtagIndex(hashtag);

            HashElement head = (HashElement) arrayHashtags[hashtagIndex];

            HashElement previous = null;

            while (head != null)
            {
                // Hashtag has been found
                if (head.key.Equals(hashtag))
                {
                    break;
                }

                // Keep moving to see if it's there
                previous = head;
                head = head.next;
            }

            if (head == null)
            {
                return null;
            }

            // Reduce the size of the array
            sizeArray--;

            // Remove the hashtag
            if (previous != null)
            {
                previous.next = head.next;
            }
            else
            {
                arrayHashtags[hashtagIndex] = head.next;
            }

            return head.post;
        }

        public model.Post GetKey(String hashtag)
        {
            // Get the head of the chain of posts for a given hashtag
            int hashtagIndex = GetHashtagIndex(hashtag);
            HashElement head = (HashElement)arrayHashtags[hashtagIndex];

            // Search the key
            while (head != null)
            {
                if (head.key.Equals(hashtag))
                {
                    return head.post;
                }
                head = head.next;
            }

            // Not found
            return null;
        }

        public void AddPost(String hashtag, model.Post post)
        {
            // Find the head for the given hashtag
            int hashtagIndex = GetHashtagIndex(hashtag);
            HashElement head = (HashElement) arrayHashtags[hashtagIndex];

            // Check if the hashtag exists
            while (head != null)
            {
                if (head.key.Equals(hashtag))
                {
                    head.post = post;
                    return;
                }
                head = head.next;
            }

            // Insert hashtag to the list of hashtags
            sizeArray++;
            head = (HashElement) arrayHashtags[hashtagIndex];
            HashElement newPostInHash = new HashElement(hashtag.ToString(), post);
            newPostInHash.next = head;
            arrayHashtags[hashtagIndex] = newPostInHash;

            // TODO: Make the table bigger if there are more hashtags
        }
    }
}
