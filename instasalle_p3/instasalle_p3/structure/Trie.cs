﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p3.structure
{
    class Trie
    {
        public TrieNode root;

        // Constructors
        public Trie (TrieNode root)
        {
            this.root = root;
        }

        public Trie ()
        {
            this.root = new TrieNode();   
        }

        public void InsertWord (string word)
        {
            int wordLength = word.Length;
            int whichLetter;
            int wordIndex;

            word = word.ToLower();

            TrieNode tempNode = root;

            // Check throughout the word for each letter
            wordIndex = 0;
            while (wordIndex < wordLength)
            {
                // Control non-letters
                if (isLetter(word[wordIndex]))
                {
                    // Determine which letter we are going to add
                    whichLetter = word[wordIndex] - 'a';

                    // Check if there is an existing letter in that level
                    if (tempNode.children[whichLetter] == null)
                    {
                        // If not, then we create a new TrieNode with the new letter
                        tempNode.children[whichLetter] = new TrieNode();

                    }

                    // We mark the new node and move forward
                    tempNode = tempNode.children[whichLetter];

                    wordIndex++;
                } else
                {
                    // Spaces as delimiter
                    if (word[wordIndex] != ' ')
                    {
                        tempNode.endWord = true;

                        // Search add new word
                        InsertWord(word.Substring(++wordIndex));
                    }

                    break;
                }
            }
            // Once we reach the end of the word, we mark it as the end
            tempNode.endWord = true;

        }

        public bool SearchWord (string word)
        {
            int wordLength = word.Length;
            int whichLetter;
            int wordIndex;

            TrieNode tempNode = root;

            word = word.ToLower();

            // Check throughout the word for each letter
            wordIndex = 0;
            while (wordIndex < wordLength)
            {
                // Determine which letter
                whichLetter = word[wordIndex] - 'a';

                // Check if there is an existing letter in that level
                if (tempNode.children[whichLetter] == null)
                {
                    return false;

                }

                // We mark the new node and move forward
                tempNode = tempNode.children[whichLetter];

                wordIndex++;
            }

            // If we reach here and it is the end of the word, it exists
            return (tempNode != null && tempNode.endWord);
        }

        private bool isLetter(char letter)
        {
            return ((letter >= 'a' && letter <= 'z') || (letter >= 'A' && letter <= 'Z'));
        }

        public int PrintAllWordSuggestions (TrieNode root, string word)
        {
            TrieNode tempNode = root;

            // Check if the prefix exists in the Trie
            int wordIndex;
            int whichLetter;
            int wordLength = word.Length;

            word = word.ToLower();


            for (wordIndex = 0; wordIndex < wordLength; wordIndex++)
            {
                whichLetter = word[wordIndex] - 'a';

                // Check if there is an existing letter in that level
                if (tempNode.children[whichLetter] == null)
                {
                    return 0;

                }
                // We mark the new node and move forward
                tempNode = tempNode.children[whichLetter];
                
            }
            // If we reach here and it is the end of the word, it exists
            bool isWord = (tempNode != null && tempNode.endWord);

            bool isLast = areAllChildrenNull(tempNode);

            // If it is a word and we have arrived at the 
            if (isWord && isLast)
            {
                Console.WriteLine(word);
                return -1;
            }

            // Is the last node, all children are null
            if (!isLast)
            {
                string prefix = word;
                Autocomplete(tempNode, prefix);
                return 1;
            }
            return 0;
        }

        public void Autocomplete (TrieNode root, string prefix)
        {
            // Caso Trivial
            // If the root is an end word, print the whole prefix
            if(root.endWord)
            {
                Console.WriteLine(prefix);
            }

            // Return if all the children are null
            if (areAllChildrenNull(root))
            {
                return;
            }

            // Caso No Ttrivial
            // Check all the children
            for (int i = 0; i < TrieNode.TOTAL_ALPHABET; i++)
            {
                
                if (root.children[i] != null)
                {
                    StringBuilder builder = new StringBuilder();

                    // Add the letter to the prefix
                    builder.Append(prefix);
                    char letter = (char)('a' + i);
                    builder.Append(letter);

                    prefix = builder.ToString();

                    // Recursive call to do it for all children
                    Autocomplete(root.children[i], prefix);
                    
                    // Remove the last letter after recursive call
                    prefix = prefix.Remove(prefix.Length - 1);
                }
            }
        }

        private bool areAllChildrenNull(TrieNode node)
        {
            bool flag = true;

            for (int i = 0; i < TrieNode.TOTAL_ALPHABET; i++)
            {
                if (node.children[i] != null)
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }

        public TrieNode Delete(TrieNode tempNode, string word, int depth = 0)
        {

            word = word.ToLower();

            if (tempNode == null)
            {
                return null;
            }

            if (depth == word.Length)
            {
                // Check if root is at the end of the word
                if (tempNode.endWord)
                {
                    tempNode.endWord = false;
                }

                // If the word is not there
                if (areAllChildrenNull(tempNode))
                {
                    tempNode = null;
                }

                return tempNode;
            }

            int whichLetter = word[depth] - 'a';
            tempNode.children[whichLetter] = Delete(tempNode.children[whichLetter], word, depth + 1);

            if (areAllChildrenNull(tempNode) && !tempNode.endWord)
            {
                tempNode = null;
            }

            return root;
        }
    } 
}
