﻿using instasalle_p3.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p3.structure
{
    class HashElement
    {
        public String key;
        public Post post;
        public HashElement next;

        // Constructor
        public HashElement(String key, Post post)
        {
            this.key = key;
            this.post = post;
        }

    }
}
