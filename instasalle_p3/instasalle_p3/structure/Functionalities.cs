﻿using instasalle_p3.graph;
using instasalle_p3.model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p3.structure
{
    public sealed class Functionalities
    {
        private static Functionalities instance = null;
        private static readonly object padlock = new object();
        

        Functionalities()
        {
            avl_tree = new AVLTree();
            trie = new Trie();
            hashtable_hashtags = new Hashtable();
            rtree_posts = new Rtree();
            limitAutocomplete = 10; // default limit
        }

        public static Functionalities Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Functionalities();
                    }
                    return instance;
                }
            }
        }
        private AVLTree avl_tree { get; set; }
        private Trie trie { get; set; }
        private Graph graph_users { get; set; }
        private Rtree rtree_posts { get; set; }
        private Hashtable hashtable_hashtags { get; set; }
        private ArrayList arrPosts { get; set; }
        private ArrayList arrUsers { get; set; }
        private int limitAutocomplete { get; set; }

        public void LoadPosts(string post)
        {
            arrPosts = JSONReader.loadPosts(post);
            int numPosts = arrPosts.Count;
            int i = 0;
            for (i = 0; i < numPosts; i++)
            {
                Post newPost = (Post)arrPosts[i];
                avl_tree.InsertAVL(newPost);
            }
            avl_tree.showInorder(avl_tree.root);
        }

        public void LoadUsers(string users)
        {
            arrUsers = JSONReader.loadUsers(users);
            graph_users = new Graph(arrUsers.Cast<User>().ToList());
        }
        
        public void VisualizeAVL()
        {
            avl_tree.showInorder(avl_tree.root);
        }

        public void VisualizeRTree()
        {
            // TODO: Implement a way to visualize the regions
        }
        
        public void VisualizeGraph()
        {
            graph_users.showGraph();
        }

        public void VisualizeTrie()
        {
            // TODO: Implement backtracking for visualization
        }

        public void VisualizeHashtable()
        {
            // TODO: Implement a way how to visualize everything. Possibly the name of the hashtag first then all the post that has
            // the same hashtag
        }

        public void AddNewUserToGraph()
        {
            // If the graph already exists
            if (graph_users != null)
            {
                User newUser = graph_users.addUser();
                // Add username to Trie
                trie.InsertWord(newUser.username);
            }
            else // If the graph still doesn't exist
            {
                graph_users = new Graph();
                User newUser = graph_users.addUser();
                trie.InsertWord(newUser.username);
            }
        }

        public void AddNewPost()
        {
            // TODO: Control if the post already exists
            // Get the last id and add one
            Post lastPost = avl_tree.GetLastPost(avl_tree.root, null);
            long newId = (long)lastPost.id + 1;

            // Publish date
            Console.Write("\nFecha de publicación \n>");
            long newPublishDate = Convert.ToInt64(Console.ReadLine());

            // Location
            double[] location = new double[2];
            Console.Write("\nUbicación: \n>Latitud:");
            location[0] = Convert.ToDouble(Console.ReadLine());
            Console.Write(">Longitud:");
            location[1] = Convert.ToDouble(Console.ReadLine()); // TODO: Control invalid location!!

            // Published by
            Console.Write("\nPublicado por \n>");
            string publishedBy = Console.ReadLine(); //TODO: Control non-existent users!!

            // Hashtags
            string optionHashtag;
            List<string> hashtags = new List<string>();
            do
            {
                Console.Write("Hashtags: [Y/N]\n>");
                optionHashtag = Convert.ToString(Console.ReadLine());
                if (!optionHashtag.Equals("Y") && !optionHashtag.Equals("N"))
                {
                    Console.WriteLine("Error, introduce una opción válida [Y/N].");
                }
                if (optionHashtag.Equals("Y"))
                {
                    Console.Write(">");
                    string newHashtag = Convert.ToString(Console.ReadLine());
                    hashtags.Add(newHashtag);
                }
            } while (!optionHashtag.Equals("N"));

            // Likes
            string optionLikes;
            List<string> likedBy = new List<string>();
            do
            {
                Console.Write("Usuarios que les gusta: [Y/N]\n>");
                optionLikes = Convert.ToString(Console.ReadLine());
                if (!optionLikes.Equals("Y") && !optionLikes.Equals("N"))
                {
                    Console.WriteLine("Error, introduce una opción válida [Y/N].");
                }
                if (optionLikes.Equals("Y"))
                {
                    Console.Write(">");
                    string newLike = Convert.ToString(Console.ReadLine());
                    likedBy.Add(newLike);
                }
            } while (!optionLikes.Equals("N"));

            Post newPost = new Post(newId, likedBy, newPublishDate, publishedBy, location, hashtags);
            avl_tree.InsertAVL(newPost);
        }

        public void updateActualLimit()
        {
            Console.Write("Límite actual: " + limitAutocomplete);
            Console.Write("Límite nuevo: \n>");
            int aux = Convert.ToInt32(Console.ReadLine());
            Console.Write("¿Estás seguro que quieres cambiar el límite a " + aux + "? [Y/N]\n>");
            string option = Console.ReadLine();
            if (option.Equals("Y") || option.Equals("y"))
            {
                limitAutocomplete = aux;
                Console.WriteLine("El límite de autocomplete ha sido actualizado.");
            }
            else if (option.Equals("N") || option.Equals("n"))
            {
                Console.WriteLine("El límite de autocomplete no ha sido actualizado.");
            }
            else
            {
                Console.WriteLine("Error, opción incorrecta. Por favor, vuelve a intentar actualizar el límite.");
            }
        }

        public string[] searchWordTrie(string wordToFind)
        {
            int result = trie.PrintAllWordSuggestions(trie.root, wordToFind);
            if (result == -1)
            {

            }
            return null;
        }

        public void RemoveUserFromGraph()
        {
            Console.Write("Nombre de usuario: \n>");
            string username = Console.ReadLine();
            graph_users.RemoveFromGraph(username);
        }

        public void RemovePost()
        {
            Console.Write("Id del post: \n>");
            int id = Convert.ToInt32(Console.ReadLine());
            avl_tree.DeleteAVL((Post)arrPosts[id]); // solo si existe en el array de posts!!!! 
            // TODO: Fix this to allow posts which already exist in the system
        }

        public void PrintSuggestions()
        {
            Console.Write("Prefix: \n>");
            string prefix = Console.ReadLine();
            Console.WriteLine("Posibles suggerencias: ");
            trie.PrintAllWordSuggestions(trie.root, prefix);
        }
    }
}
