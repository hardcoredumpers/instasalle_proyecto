﻿using instasalle_p3.model;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p3
{
    class JSONReader
    {
        public static ArrayList loadPosts (String filename)
        {
            ArrayList arrayList = new ArrayList();

            try
            {
                string dir = System.IO.Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                string final_path = dir + "\\..\\..\\resources" + filename;
                StreamReader sr = new StreamReader(final_path);
                string json = sr.ReadToEnd();
                var file = JsonConvert.DeserializeObject<List<Post>>(json);
                for (int i =0; i < file.Count(); i++)
                {
                    arrayList.Add(file[i]);
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Error! File doesn't exist.");
            }
            return arrayList;
        }

        public static ArrayList loadUsers(String filename)
        {
            ArrayList arrayList = new ArrayList();

            try
            {
                string dir = System.IO.Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                string final_path = dir + "\\..\\..\\resources" + filename;
                StreamReader sr = new StreamReader(final_path);
                string json = sr.ReadToEnd();
                var file = JsonConvert.DeserializeObject<List<User>>(json);
                for (int i = 0; i < file.Count(); i++)
                {
                    arrayList.Add(file[i]);
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Error! File doesn't exist.");
            }
            return arrayList;
        }
    }
}
