﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p3.model
{
    class Post
    {
        public long id { get; set; }
        public List<string> liked_by { get; set; }
        public long published_when { get; set; }
        public string published_by { get; set; }
        public double[] location { get; set; }
        public List<string> hashtags { get; set; }

        public Post()
        {

        }

        public Post(long id, List<string> liked_by, long published_when, string published_by, double[] location, List<string> hashtags)
        {
            this.id = id;
            this.liked_by = liked_by;
            this.published_when = published_when;
            this.published_by = published_by;
            this.location = location;
            this.hashtags = hashtags;
        }

        public Post(long id, long published_when, string published_by, double[] location)
        {
            this.id = id;
            this.published_when = published_when;
            this.published_by = published_by;
            this.location = location;
        }
    }
}
