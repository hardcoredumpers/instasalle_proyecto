﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p3.model
{
    class User
    {
        public string username { get; set; }
        public long creation { get; set; }
        public List<string> to_follow { get; set; }

        public User() { }

        public User(String username, long creation, List<string> to_follow)
        {
            this.username = username;
            this.creation = creation;
            this.to_follow = to_follow;

        }


        public User(string username, long creation)
        {
            this.username = username;
            this.creation = creation;
            this.to_follow = new List<string>();
        }

        public void addFollowing(string user)
        {
            to_follow.Add(user);
        }
    }
}
