﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p3.graph
{
    class Edge
    {
        public Vertex user_following { get; set; }
        public Vertex user_followed { get; set; }

        public Edge (Vertex user_following, Vertex user_followed) 
        {
            this.user_following = user_following;
            this.user_followed = user_followed;
        }
    
    }
}
