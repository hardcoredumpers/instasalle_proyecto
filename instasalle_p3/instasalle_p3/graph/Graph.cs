﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using instasalle_p3.model;

namespace instasalle_p3.graph
{
    class Graph
    {
        public List<Vertex> vertexs { get; set; }

        public Graph()
        {
            vertexs = new List<Vertex>();
        }

        public Graph(List<User> users) {
            this.vertexs = new List<Vertex>();
            for(int i=0; i < users.Count; i++) {
                User user = users[i];
                List<Edge> user_connexions = new List<Edge>();
                for(int j=0; j<users[i].to_follow.Count; j++)
                {
                    Edge edge = new Edge (new Vertex(user), new Vertex(findUser(users, users[i].to_follow[j])));
                    user_connexions.Add(edge);
                }
                Vertex vertex = new Vertex(users[i], user_connexions);
                this.vertexs.Add(vertex);
            }
        }

        public User findUser(List<User> users, String name_user)
        {
            User user = new User();
            for (int i=0; i<users.Count; i++)
            {
                if (users[i].username.Equals(name_user))
                {
                    user = users[i];
                }
            }
            return user;
        }

        public Vertex findVertex(String username)
        {
            Vertex vertex = new Vertex();
            for (int i = 0; i < this.vertexs.Count; i++)
            {
                if (this.vertexs[i].user.username.Equals(username))
                {
                    vertex = this.vertexs[i];
                }
            }
            return vertex;
        }

        public User addUser()
        {
            User new_user = new User();
            Vertex new_vertex = new Vertex();
            long creation_long = 0;
            String username;
            List<String> list_followed = new List<String>();
            List<Edge> user_connexions = new List<Edge>();
            Console.WriteLine("Nombre de usuario: ");
            username = Console.ReadLine();
            while (userExists(username))
            {
                Console.WriteLine("Error, el nombre de usuario ya existe. Por favor, introduce el nombre de usuario correcto: \n>");
                //Console.WriteLine("Sorry, but that username already exists. Please, enter the username of the user you want to add: ");
                username = Console.ReadLine();
            }
            Console.Write("Fecha de creación: \n>");
            //Console.WriteLine("Date of creation: ");
            String creation = Console.ReadLine();
            Boolean parse = false;
            while (!parse)
            {
                try
                {
                    creation_long = Convert.ToInt64(creation);
                    parse = true;
                }
                catch (FormatException)
                {
                    //Console.WriteLine("Please, enter a valid number: ");
                    Console.WriteLine("Por favor, introduce un número válido: ");
                    creation = Console.ReadLine();
                }
            }
            Boolean wants_to_follow = true;
            while (!noUsers() && wants_to_follow)
            {
                //Console.WriteLine("Users that will be followed  by " + username + " [Y / N]:");
                Console.Write("Usuarios que seguirá por " + username + ": [Y/N]\n>");
                String option = Console.ReadLine();
                while(!(option.Equals("N") || option.Equals("Y") || option.Equals("n") || option.Equals("y")))
                {
                    //Console.WriteLine("Please select a valid option between 'Y' and 'N'.\nUsers that will be followed  by " + username + " [Y/N]:");
                    Console.WriteLine("Por favor, selecciona una opcíon válido Y/N.");
                    Console.Write("Usuarios que seguirá por " + username + ": [Y/N]\n>");
                    option = Console.ReadLine();
                }
                if (option.Equals("Y") || option.Equals("y"))
                {
                    String user_followed = Console.ReadLine();
                    while (!userExists(user_followed) || alreadyFollows(list_followed, user_followed))
                    {
                        if (!userExists(user_followed))
                        {
                            //Console.WriteLine("That username is not registered. Please enter a valid username to follow:");
                            Console.Write("El nombre de usuario no está registrad. Por favor, introduce un nombre de usuario a seguir: \n>");
                        }
                        else
                        {
                            //Console.WriteLine("The new user already follows this user. Please enter a valid username to follow:");
                            Console.Write("El nuevo usuario ya está siguiendo al usuario introducido. " +
                                "Por favor, introduce un nombre de usuario a seguir: \n>");
                        }
                        user_followed = Console.ReadLine();
                    }
                    list_followed.Add(user_followed);
                } else
                {
                    wants_to_follow = false;
                }
            }
            new_user = new User(username, creation_long, list_followed);
            for (int i = 0; i < list_followed.Count; i++)
            {
                Edge edge = new Edge(new Vertex(new_user), findVertex(list_followed[i]));
                user_connexions.Add(edge);
            }
            new_vertex = new Vertex(new_user, user_connexions);
            this.vertexs.Add(new_vertex);
            //Console.WriteLine("The user [" + username + "] has been succesfully added from the system.");
            Console.WriteLine("El usuario [" + username + "] ya ha sido añadido al sistema.");

            return new_user;
        }

        public Boolean alreadyFollows(List<String> list_followed, String new_followed)
        {
            Boolean already = false;
            for(int i=0; i<list_followed.Count; i++)
            {
                if (list_followed[i].Equals(new_followed))
                {
                    already = true;
                }
            }
            return already;
        }

        public void removeUser()
        {
            if(noUsers())
            {
                Console.WriteLine("Lo sentimos, pero no hay ningún usuario registrado en InstaSalle actualmente.");
            } else
            {
                String username = selectUser();
                RemoveFromGraph(username);
                Console.WriteLine("El usuario [" + username + "] ha sido eliminado correctamente del sistema.");
            }

        }

        public Boolean noUsers()
        {
            return this.vertexs.Count <= 0;
        }

        public void RemoveFromGraph(String username)
        {
            Boolean found = false;
            for (int i = 0; i < this.vertexs.Count && !found; i++)
            {
                if (this.vertexs[i].user.username.Equals(username))
                {
                    this.vertexs.RemoveAt(i);
                    found = true;
                }
            }
            for (int i = 0; i < this.vertexs.Count; i++)
            {
                for (int j = 0; j < this.vertexs[i].following.Count; j++)
                {
                    if (this.vertexs[i].following[j].user_followed.user.username.Equals(username))
                    {
                        this.vertexs[i].following.RemoveAt(j);
                    }
                }
            }
            
        }

        public String selectUser()
        {
            Console.WriteLine("Por favor, introduce el username del usuario que deseas eliminar: ");
            String username = Console.ReadLine();
            Boolean found = userExists(username);
            while (!found)
            {
                Console.WriteLine("Lo sentimos, pero ese usuario no existe. Por favor, introduce el username del usuario que deseas eliminar: ");
                username = Console.ReadLine();
                found = userExists(username);
            }
            return username;

        }

        public Boolean userExists(String username)
        {
            Boolean exists = false;
            for (int i = 0; i < this.vertexs.Count; i++)
            {
                if (this.vertexs[i].user.username.Equals(username))
                {
                    exists = true;
                }
            }
            return exists;
        }

        public void showGraph()
        {
            Console.WriteLine("\nEste es el graph de usuarios: \n");
            for (int i=0; i<this.vertexs.Count; i++)
            {
                Console.WriteLine("Vértice: " + this.vertexs[i].user.username + "\nSigue a: ");
                for (int j = 0; j < this.vertexs[i].following.Count; j++)
                {
                    if (j == this.vertexs[i].following.Count - 1)
                    {
                        Console.Write(this.vertexs[i].following[j].user_followed.user.username + "\n\n");
                    }
                    else
                    {
                        Console.Write(this.vertexs[i].following[j].user_followed.user.username + ", ");
                    }
                }
                
            }
            Console.WriteLine(" ");
        }

    }

 
}
