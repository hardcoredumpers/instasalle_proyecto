﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using instasalle_p3.model;

namespace instasalle_p3.graph
{
    class Vertex
    {
        public User user { get; set; }
        public List<Edge> following { get; set; }

        public Vertex(){}

        public Vertex (User user)
        {
            this.user = user;
        }

        public Vertex(User user, List<Edge> following)
        {
            this.user = user;
            this.following = following;
        }

        public void addFollowing (List<Edge> following)
        {
            this.following = following;
        }
    }
}
